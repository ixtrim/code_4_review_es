(function ($) {
 $(document).ready(function () {

  // Menu Burger
  var Menu = {
   el: {
    ham: $('.header__nav-trigger__icon'),
    menuTop: $('.line-top'),
    menuMiddle: $('.line-middle'),
    menuBottom: $('.line-bottom'),
    menuContainer: $('.desktop-menu')
   },
   init: function () {
    Menu.bindUIactions();
   },
   bindUIactions: function () {
    Menu.el.ham
     .on(
      'click',
      function (event) {
       Menu.activateMenu(event);
       event.preventDefault();
      }
     );
   },
   activateMenu: function () {
    Menu.el.menuTop.toggleClass('line-top-click');
    Menu.el.menuMiddle.toggleClass('line-middle-click');
    Menu.el.menuBottom.toggleClass('line-bottom-click');
    Menu.el.menuContainer.toggleClass('desktop-menu--extended');
   }
  };

  // Map
  var Map = {
    el: {
     ham: $('.header__bottom__map--trigger'),
     mapContainer: $('.desktop-map')
    },
    init: function () {
     Map.bindUIactions();
    },
    bindUIactions: function () {
     Map.el.ham
      .on(
       'click',
       function (event) {
        Map.activateMenu(event);
        event.preventDefault();
       }
      );
    },
    activateMenu: function () {
      Map.el.ham.toggleClass('header__bottom__map--trigger__active');
      Map.el.mapContainer.toggleClass('desktop-map--extended');
    }
   };


  Menu.init();
  Map.init();
  
 });

})(jQuery);