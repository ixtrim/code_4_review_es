<?php
/**
 * Ixtrim-Theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

define('WP_SCSS_ALWAYS_RECOMPILE', true);

if (! class_exists( 'Timber' )) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});

	add_filter('template_include', function( $template ) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

/**
 * Sets the directories to find .twig files
 */
Timber::$dirname = [
	'templates', 
	'views', 
	'woocommerce-views'
];

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class IxtrimSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action('after_setup_theme', array($this, 'theme_supports'));
		add_filter('timber/twig', array($this, 'add_to_twig'));
		add_action('init', array($this, 'register_post_types'));
    add_action('init', array($this, 'register_taxonomies'));
		add_action('widgets_init', array($this, 'widgets_init'), 100); 

		/** Disable admin bar */
		add_filter('show_admin_bar', '__return_false');

		/** Enqueue assets */
		add_action('wp_enqueue_scripts', array($this, 'enqueueAssets'), 15);

		/** Add theme acf settings page to backend. */
    if (function_exists('acf_add_options_page')) {
			add_action('acf/init', array($this, 'options_page_acf_init'));
		}
		add_action('acf/init', array($this, 'my_acf_init'));

    /** Add variables for Timber */
		add_filter('timber_context', array($this, 'add_timber_context_options'));

		/** Add Google Map API */
		add_filter('acf/init', array($this, 'my_acf_init'));

		/** Accept SVG files */
		add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

		global $wp_version;
		if ( $wp_version !== '4.7.1' ) {
			return $data;
		}

		$filetype = wp_check_filetype( $filename, $mimes );

		return [
			'ext'             => $filetype['ext'],
			'type'            => $filetype['type'],
			'proper_filename' => $data['proper_filename']
		];

		}, 10, 4 );
		add_filter('upload_mimes', array($this, 'cc_mime_types'));
		add_action('admin_head', array($this, 'fix_svg'));
		
		/** Add new image sizes */
		add_image_size('square', 767, 767, true);
		add_image_size('rectangleX', 767, 432, true);
		add_image_size('rectangleY', 510, 767, true);
		add_image_size('galThumb', 350, 235, true);
		add_image_size('hero', 1300, 600, true);

		@ini_set( 'upload_max_size' , '64M' );
		@ini_set( 'post_max_size', '64M');
		@ini_set( 'max_execution_time', '300' );

		parent::__construct();
  }

	/**
   * Enqueues styles and scripts.
   *
   * @implements wp_enqueue_scripts
   */
  public function enqueueAssets() {
		wp_enqueue_style('theme/layout', static::getBaseUrl() . '/assets/dist/css/theme.css');
		wp_enqueue_script('theme/main', static::getBaseUrl() . '/assets/dist/js/theme.js', array('jquery'), '1.0.0', true );

		// Fancybox
		wp_enqueue_script('theme/jquery', static::getBaseUrl() . '/assets/dist/libraries/fancybox/jquery.min.js', array('jquery'), '3.5.1');
		wp_enqueue_style('theme/fancybox-css', static::getBaseUrl() . '/assets/dist/libraries/fancybox/jquery.fancybox.min.css');
		wp_enqueue_script('theme/fancybox', static::getBaseUrl() . '/assets/dist/libraries/fancybox/jquery.fancybox.min.js', array('jquery'), '3.5.7');

		// Icofont
		wp_enqueue_style('icon-font-css', static::getBaseUrl() . '/assets/dist/icofont/icofont.css');

		// Swiper
		wp_enqueue_script('theme/swiper', static::getBaseUrl() . '/assets/dist/libraries/swiper/swiper.min.js','','',true);
		wp_enqueue_style('theme/swiper-css', static::getBaseUrl() . '/assets/dist/libraries/swiper/swiper.min.css');
	}

	/**
   * Allow SVG files
   */
	public function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	public function fix_svg() {
		echo '<style type="text/css">
			  .attachment-266x266, .thumbnail img {
				   width: 100% !important;
				   height: auto !important;
			  }
			  </style>';
	  }
	   
	
	/**
   * Returns the base URL of this theme.
   *
   * @return string
   */
  public static function getBaseUrl() {
    if (!isset(parent::$baseUrl)) {
      $baseUrl = get_stylesheet_directory_uri();
    }
    return $baseUrl;
	}

	/**
   * Adds google map api
   *
   * @implements acf/init
   */
	public static function my_acf_init() {
		acf_update_setting('google_api_key', 'AIzaSyDT2FicpaGItrnnECigLQEUChe6mVx9xng');
	}

  /**
   * Adds timber context variables.
   *
   * @implements timber_context
   */
  public static function add_timber_context_options( $context ) {
		$context['menuPrimary'] = new TimberMenu('menu-main');
		$context['menuSecond'] = new TimberMenu('menu-foot');
		$context['menuThird'] = new TimberMenu('menu-foot-second');
    	//$context['site'] = $this;

		// ACF Theme Options Page
		$queried_object = get_queried_object();
		$context['options'] = get_fields('options');
		$context['specialText'] = get_field('special_text', $queried_object);
		//$context['title'] = $queried_object->name;
		//$context['slug'] = $queried_object->slug;
		//$context['description'] = $queried_object->description;
		//$context['taxonomy'] = $queried_object->taxonomy;

    // Yoast Breadcrumbs Module
    if (function_exists('yoast_breadcrumb') && !is_home() && !is_front_page() && !is_page_template('contact-page.php')) {
      $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">','</nav>', false);
    }
  
    return $context;
	}
  
	/**
	 * @implements widgets_init
	 */
	public function widgets_init() {
		register_sidebar([
		'id'            => 'primary_sidebar',
		'name'          => __('Primary Sidebar', 'ixtrim-theme'),
		'before_widget' => '<aside class="primary-sidebar-module %2$s" id="%1$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
		]);
		register_sidebar([
		'id'            => 'secondary_sidebar',
		'name'          => __('Secondary Sidebar', 'ixtrim-theme'),
		'before_widget' => '<aside class="secondary-sidebar-module %2$s" id="%1$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
		]);
	}
	
	/**
   * Add theme acf settings page to backend.
   *
   * @implements options_page_acf_init
   */
	public function options_page_acf_init() {
		acf_add_options_page([
		'page_title' => __('IxtrimTheme Settings', 'ixtrim-theme'),
		'menu_title' => __('IxtrimTheme', 'ixtrim-theme'),
		'menu_slug' => 'ixtrim-theme-settings',
		'capability' => 'edit_posts',
		'redirect' => FALSE,
		]);
	}

	/** This is where you can register custom post types. */
	public function register_post_types() {
		register_post_type('testimonial',
			array(
				'labels' => [
					'name' => _x('Testimonials', 'taxonomy', 'ixtrim-theme'),
					'singular_name' => _x('Testimonials', 'taxonomy', 'ixtrim-theme'),
				],
				'public' => true,
				'has_archive' => false,
				'rewrite' => [
					'slug' => 'testimonials'
				],
				'show_ui' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'query_var' => true,
				'supports' => [
					'title', 
					'editor', 
					'excerpt', 
					'custom-fields', 
					'thumbnail'
				],
        'menu_position'       => 5,
				'can_export' => true,
				'menu_icon' => 'dashicons-businesswoman',
			)
		);
	}

	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {
		register_taxonomy('testimonials', 'testimonial', [
      	'hierarchical' => true,  
			'label' => 'Category',
			'query_var' => true,
			'rewrite' => [
				'slug' => 'testimonial',
				'with_front' => false,
			]
		]);
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array($this, 'myfoo' ) ) );
		return $twig;
	}

}

new IxtrimSite();
