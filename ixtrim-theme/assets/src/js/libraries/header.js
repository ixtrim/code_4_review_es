(function ($) {
 $(document).ready(function () {

  // Menu Burger
  var Menu = {
   el: {
    ham: $('.header__menu__mobile'),
    menuContainer: $('.mobile-menu')
   },
   init: function () {
    Menu.bindUIactions();
   },
   bindUIactions: function () {
    Menu.el.ham
     .on(
      'click',
      function (event) {
       Menu.activateMenu(event);
       event.preventDefault();
      }
     );
   },
   activateMenu: function () {
    Menu.el.ham.toggleClass('opened');
    Menu.el.menuContainer.toggleClass('opened');
   }
  };

  var MenuClose = {
   el: {
    ham: $('.mobile-menu__close'),
    menuContainer: $('.mobile-menu')
   },
   init: function () {
    MenuClose.bindUIactions();
   },
   bindUIactions: function () {
    MenuClose.el.ham
     .on(
      'click',
      function (event) {
       MenuClose.activateMenu(event);
       event.preventDefault();
      }
     );
   },
   activateMenu: function () {
    MenuClose.el.ham.toggleClass('opened');
    MenuClose.el.menuContainer.toggleClass('opened');
   }
  };

  Menu.init();
  MenuClose.init();
  
 });

})(jQuery);