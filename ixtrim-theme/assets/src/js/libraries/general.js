(function($) {
  $(document).ready(function(){

    if ($('.hero-slider').length) {
      var swiper = new Swiper('.hero-slider', {
        init: false,
        loop: true,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        autoplay: {
          delay: 3500,
          disableOnInteraction: false,
        },
      });
      swiper.on('init', function() {
        $('.swiper-slide-active .swiper-slide__text').addClass('slideTextVisible');
      });
      swiper.on('slideChangeTransitionStart', function() {
        $('.swiper-slide__text').removeClass('slideTextVisible');
      });
      swiper.on('slideChangeTransitionEnd', function() {
        $('.swiper-slide-active .swiper-slide__text').addClass('slideTextVisible');
      });
      
      swiper.init();
    }

    if ($('.testimonials-slider').length) {
      var swiperTestimonials = new Swiper('.testimonials-slider', {
        init: false,
        loop: true,
        autoHeight: true,
        spaceBetween: 20,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        autoplay: {
          delay: 3500,
          disableOnInteraction: false,
        },
      });
      
      swiperTestimonials.init();
    }

    if ($('#formState').length) {
      $('#formState').prepend("<option value='' selected='selected'>Choose state *</option>");
    }
    
  });
}) (jQuery);