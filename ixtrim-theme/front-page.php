<?php
/**
 * The default template file
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$queried_object = get_queried_object();
$context = Timber::context();
$post = new TimberPost();
$context['post'] = $post;

// Timber variables for use in twig file
$context['builder'] = get_field('page_builder', $queried_object);
if ( function_exists( 'yoast_breadcrumb' ) ) {
  $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">','</nav>', false );
}

// Assign twig file to that template
$templates = array('front-page.twig');

Timber::render($templates, $context);