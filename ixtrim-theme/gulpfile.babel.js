import wpPot from "gulp-wp-pot";
import info from "./package.json";
import zip from "gulp-zip";
import browserSync from "browser-sync";
import named from 'vinyl-named';
import webpack from 'webpack-stream';
import del from 'del';
import imagemin from 'gulp-imagemin';
import postcss from 'gulp-postcss';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'autoprefixer';
import { src, dest, watch, series, parallel} from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
const PRODUCTION = yargs.argv.prod;

export const styles = () => {
  return src(['assets/src/scss/theme.scss','assets/src/scss/backend.scss'])
    .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpif(PRODUCTION, postcss([ autoprefixer ])))
    .pipe(gulpif(PRODUCTION, cleanCss({compatibility:'ie8'})))
    .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
    .pipe(dest('assets/dist/css'))
    .pipe(server.stream());
}

export const images = () => {
  return src('assets/src/images/**/*.{jpg,jpeg,png,svg,gif}')
    .pipe(gulpif(PRODUCTION, imagemin()))
    .pipe(dest('assets/dist/images'));
}

export const copy = () => {
  return src(['assets/src/**/*','!assets/src/{images,js,scss}','!assets/src/{images,js,scss}/**/*'])
    .pipe(dest('assets/dist'));
}

export const clean = () => del(['assets/dist']);

export const scripts = () => {
  return src(['assets/src/js/theme.js','assets/src/js/admin.js'])
    .pipe(named())
    .pipe(webpack({
      module: {
        rules: [
          {
            test: /\.js$/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: []
            }
          }
        }
      ]
    },
    mode: PRODUCTION ? 'production' : 'development',
    devtool: !PRODUCTION ? 'inline-source-map' : false,
    output: {
      filename: '[name].js'
    },
    externals: {
      jquery: 'jQuery'
    },
  }))
  .pipe(dest('assets/dist/js'));
}

const server = browserSync.create();
export const serve = done => {
  server.init({
    proxy: "http://jimmyauto.imm/"
  });
  done();
};
export const reload = done => {
  server.reload();
  done();
};

export const pot = () => {
  return src("**/*.php")
  .pipe(
      wpPot({
        domain: "_ixtrim_theme",
        package: info.name
      })
    )
  .pipe(dest(`languages/${info.name}.pot`));
};

export const watchForChanges = () => {
  watch('assets/src/scss/**/*.scss', styles);
  watch('assets/src/images/**/*.{jpg,jpeg,png,svg,gif}', series(images, reload));
  watch(['assets/src/**/*','!assets/src/{images,js,scss}','!assets/src/{images,js,scss}/**/*'], series(copy, reload));
  watch('assets/src/js/**/*.js', series(scripts, reload));
  watch(['**/*.php','**/*.twig'], reload);
}

export const compress = () => {
  return src([
    "**/*",
    "!node_modules{,/**}",
    "!bundled{,/**}",
    "!assets/src{,/**}",
    "!.babelrc",
    "!.gitignore",
    "!gulpfile.babel.js",
    "!package.json",
    "!package-lock.json",
    ])
    .pipe(zip(`../../${info.name}.zip`))
    .pipe(dest('bundled'));
  };

export const dev = series(clean, parallel(styles, images, copy, scripts), serve, watchForChanges)
export const build = series(clean, parallel(styles, images, copy, scripts), pot,compress)

export default dev;
