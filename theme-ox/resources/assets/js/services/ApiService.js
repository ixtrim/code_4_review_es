import { customerAxios }  from "./interceptor";

class ApiService {
    static getLocations(locationId) {
        const params = {};
        if (locationId) params.locationId = locationId;
        return customerAxios
            .get("location-info", { params })
            .then(res => {
                return res.data;
            })
            .catch(e => {
                console.log(e);
            });
    }

    static updateUserPassword(data) {
        return customerAxios
            .post("custom/user/update-password", {
                ...data,
            });
    }

    static authenticateCustomer(data) {
        return customerAxios
            .post("custom/user/authenticate-customer", {
                ...data,
            });
    }
}

export default ApiService;
