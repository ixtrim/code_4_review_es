// todo - try to move booking promises etc here to this service file by file
import { bookingAxios } from "./interceptor";

class BookingService {
  static getGroupedCoursesRequest() {
    return bookingAxios.get(
      `${window.wpApiSettings.root}custom/v1/grouped-products`
    );
  }
  static getGroupedCourseChildrenRequest(groupedProductId) {
    return bookingAxios.get(
      `${window.wpApiSettings.root}custom/v1/grouped-products/${groupedProductId}/children`
    );
  }
  static getProductVariations(productId, attributeValue) {
    return bookingAxios.get(
      `${
        window.wpApiSettings.root
      }wc/v3/products/${productId}/variations?search="${encodeURIComponent(
        attributeValue
      )}"`
    );
  }
  static postDepositRequest(payload) {
    return bookingAxios.post(`${window.wpApiSettings.root}wc/v3/deposit`,payload);
  }
  static postPaymentRequest(payload) {
    return bookingAxios.post(`${window.wpApiSettings.root}wc/v3/payment`,payload);
  }
}

export default BookingService;
