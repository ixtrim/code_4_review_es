import axios from "axios";

axios.defaults.baseURL = "../wp-json/wp/v2/";

const customerAxios = axios.create({
  withCredentials: true,
  headers: {
    /*eslint-disable */
    "X-WP-Nonce": wpApiSettings.nonce || false,
    Authorization: `Basic ${env.WOOCOMMERCE_TOKEN}`
  }
})
const bookingAxios = axios.create({
  headers: {
    /*eslint-disable */
    "X-WP-Nonce": wpApiSettings.nonce || false,
    Authorization: `Basic ${env.WOOCOMMERCE_TOKEN}`
  }
})
const accountAxios = axios.create({
  baseURL: "../wp-json/wp/v2/custom/account/",
  headers: {
    /*eslint-disable */
    "X-WP-Nonce": wpApiSettings.nonce || false,
    Authorization: `Basic ${env.WOOCOMMERCE_TOKEN}`
  }
});

// When password is changed we need to replace the nonce both in axios and wpApiSettings
const nonceUpdateResponseInterceptor = response => {
  if (response && response.data && response.data.newNonce) {
    wpApiSettings.nonce = response.data.newNonce;
    customerAxios.defaults.headers = {
      ...customerAxios.defaults.headers,
      "X-WP-Nonce": response.data.newNonce,
    };
    accountAxios.defaults.headers = {
      ...accountAxios.defaults.headers,
      "X-WP-Nonce": response.data.newNonce,
    };
    bookingAxios.defaults.headers = {
      ...bookingAxios.defaults.headers,
      "X-WP-Nonce": response.data.newNonce,
    };
  }
  
  return response;
}
customerAxios.interceptors.response.use(nonceUpdateResponseInterceptor, error => Promise.reject(error));
accountAxios.interceptors.response.use(nonceUpdateResponseInterceptor, error => Promise.reject(error));
bookingAxios.interceptors.response.use(nonceUpdateResponseInterceptor, error => Promise.reject(error));

export { customerAxios, accountAxios, bookingAxios };
