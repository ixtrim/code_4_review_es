import { accountAxios, bookingAxios } from "./interceptor";
import Util from "../utils";

class AccountService {
  static getUserTasks() {
    return accountAxios.get("tasks");
  }
  static getUserBookingCourses() {
    return accountAxios.get("orders");
  }
  static getAccountUserdata() {
    return accountAxios.get("userdata");
  }
  static getSingleOrder(orderId, productId) {
    return accountAxios.get(`orders/${orderId}/${productId}`);
  }

  static getSingleOrderStudentDetails(orderId, productId) {
    return accountAxios.get(`orders/${orderId}/${productId}/student-details`);
  }

  static patchStudentDetailsRequest(orderId, productId, payload) {
    return accountAxios.patch(
      `orders/${orderId}/${productId}/student-details`,
      payload
    );
  }

  static getSingleOrderMedicalInformation(orderId, productId) {
    return accountAxios.get(
      `orders/${orderId}/${productId}/medical-information`
    );
  }

  static patchSingleOrderMedicalInformation(orderId, productId, payload) {
    return accountAxios.patch(
      `orders/${orderId}/${productId}/medical-information`,
      payload
    );
  }

  static getSingleOrderAirportInformation(orderId, productId) {
    return accountAxios
      .get(`orders/${orderId}/${productId}/airport-information`)
      .then(res => {
        // Change some arrays to nulls for the select dropdown values
        // Why? - https://support.advancedcustomfields.com/forums/topic/get_field-returning-array-for-select-when-no-value-exists-in-database/
        res.data.data.arrival = Util.nullifyKeys(
          ["airport", "terminal", "from", "type_of_transfer"],
          res.data.data.arrival
        );
        res.data.data.departure = Util.nullifyKeys(
          ["airport", "terminal", "to"],
          res.data.data.departure
        );
        return res;
      });
  }

  static patchSingleOrderAirportInformation(orderId, productId, payload) {
    return accountAxios.patch(
      `orders/${orderId}/${productId}/airport-information`,
      payload
    );
  }

  static getSingleOrderAccomodationInformation(orderId, productId) {
    return accountAxios.get(
      `orders/${orderId}/${productId}/accomodation-information`
    );
  }

  static patchSingleOrderAccomodationInformation(orderId, productId, payload) {
    return accountAxios.patch(
      `orders/${orderId}/${productId}/accomodation-information`,
      payload
    );
  }

  static getCourseTasks(orderId, productId) {
    return accountAxios.get(`course/${orderId}/${productId}/tasks`);
  }

  static markTaskAsCompleted(taskId) {
    return accountAxios.patch(`tasks/${taskId}/mark-as-completed`);
  }

  /**
   * parent / guardian details - in fact WP user
   */

  static getParentDetailsRequest() {
    return accountAxios.get("parent");
  }

  static patchParentDetailsRequest(payload) {
    return accountAxios.patch("parent", payload);
  }

  static postDepositPayment(payload) {
    return bookingAxios.post(
      `${window.wpApiSettings.root}wc/v3/deposit`,
      payload
    );
  }
  static postTotalAmountPayment(payload) {
    return bookingAxios.post(
      `${window.wpApiSettings.root}wc/v3/payment`,
      payload
    );
  }

  /**
   * Past orders
   */
  static getPastOrders() {
    return accountAxios.get("past-orders");
  }

  static getPastCourse(orderId, productId, variationId) {
    return accountAxios.get(
      "past-orders/" + orderId + "/" + productId + "/" + variationId
    );
  }

  static postPastCourseReview(payload, orderId, productId, variationId) {
    return accountAxios.post(
      "past-orders/" +
        orderId +
        "/" +
        productId +
        "/" +
        variationId +
        "/review",
      payload
    );
  }

  static getReportCard(orderId, productId, variationId, type = null) {
    let url =
      "report-card/" + orderId + "/" + productId + "/" + variationId + "/";
    url = !type ? url : url + "?type=" + type;
    return accountAxios.get(url);
  }

  /**
   * optional extras
   */
  static getOptionalExtras(orderId, productVariationId) {
    return accountAxios.get(
      `orders/${orderId}/${productVariationId}/optional-extras`
    );
  }
}

export default AccountService;
