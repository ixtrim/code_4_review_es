import $ from "jquery";
import "./lib/para";
import "./lib/viewport";

import vuelidate from "vuelidate";
import VueRouter from "vue-router";

import MenuActions from "./actions/menu";

import VideoPlayer from "./page-builder/videoPlayer";
import Gallery from "./page-builder/gallery";
import Tutors from "./page-builder/tutors";
import TestimonialsCarousel from "./page-builder/testimonials";
import AvailableSubjects from "./page-builder/availableSubjects";
import RelatedPosts from "./page-builder/related-posts";

import Courses from "./pages/homepage/courses";
import Subjects from "./pages/homepage/subjects";
import Locations from "./pages/homepage/locations";
import News from "./pages/homepage/news";
import Favourites from "./pages/homepage/favourites";

import GlobalSettings from "./actions/global";

import Nav from "./animations/nav";
import SideActions from "./animations/sideActions";
import HomeSlider from "./animations/homeSlider";
import singlePost from "./animations/singlePost";

import Accordion from "./actions/accordion";

import Vue from "vue/dist/vue.js";
import OurCourses from "./vue/Courses/Courses.vue";
import OurSubjects from "./vue/Subjects/Subjects.vue";
import Filters from "./vue/Courses/Filters.vue";
import NewsListing from "./vue/News.vue";
import LocationsListing from "./vue/Locations.vue";
import Testimonials from "./vue/Testimonials/Testimonials.vue";
import EnglishTest from "./vue/EnglishTest/englishTest.vue";
import BoxIntegration from "./vue/BoxIntegration.vue";

import Booking from "./vue/Booking/Booking.vue";
import BookingForm from "./vue/Booking/BookingForm.vue";
import BookingStart from "./vue/Booking/BookingStart.vue";
import Account from "./vue/Account/Account";
import Step01 from "./vue/Booking/steps/Step01.vue";
import Step02 from "./vue/Booking/steps/Step02.vue";
import Step03 from "./vue/Booking/steps/Step03.vue";
import Step04 from "./vue/Booking/steps/Step04.vue";
import Step05 from "./vue/Booking/steps/Step05.vue";
import bookingSummary from "./vue/Booking/steps/bookingSummary.vue";
import AccountService from "./services/AccountService";

import MyBookings from "./vue/Account/elements/MyBookings";
import MyBookingsMain from "./vue/Account/elements/MyBooking/Main";
import ParentDetails from "./vue/Account/elements/ParentDetails";
import AccountDetails from "./vue/Account/elements/AccountDetails";
import StudentDetails from "./vue/Account/elements/MyBooking/StudentDetails";
import PaymentDetails from "./vue/Account/elements/MyBooking/PaymentDetails";
import OptionalExtras from "./vue/Account/elements/MyBooking/OptionalExtras";
import MedicalInformation from "./vue/Account/elements/MyBooking/MedicalInformation";
import AirportTransfer from "./vue/Account/elements/MyBooking/AirportTransfer";
import AccommodationPreferences from "./vue/Account/elements/MyBooking/AccommodationPreferences";
import CourseDetail from "./vue/Account/elements/MyBooking/CourseDetail";
import Loader from "./vue/directives/Loader";

import PastCourses from "./vue/Account/elements/PastCourses";
import PastCoursesMain from "./vue/Account/elements/PastCourses/Main";
import ReportCard from "./vue/Account/elements/PastCourses/ReportCard";
import PastCourse from "./vue/Account/elements/PastCourses/PastCourse";

import Vuebar from "vuebar";
import Notifications from "vue-notification";
import vSelect from "vue-select";

window.$ = $;
window.jQuery = $;

Vue.use(vuelidate);
Vue.use(VueRouter);
Vue.use(Vuebar);
Vue.use(Notifications);

Vue.directive("loader", Loader);

const myAccountRoutes = [
  {
    base: "/my-account",
    path: "/",
    redirect: "/my-bookings",
    component: Account,
    children: [
      {
        path: "my-bookings",
        component: MyBookings,
        redirect: "/my-bookings/overview",
        children: [
          {
            path: "overview",
            component: MyBookingsMain,
          },
          {
            path: ":orderId/:variationId/student-details/:taskId",
            component: StudentDetails,
          },
          {
            path: ":orderId/:variationId/passport-number/:taskId",
            component: StudentDetails,
          },
          {
            path: ":orderId/:variationId/medical-information/:taskId",
            component: MedicalInformation,
          },
          {
            path: ":orderId/:variationId/airport-transfer/:taskId",
            component: AirportTransfer,
          },
          {
            path: ":orderId/:variationId/accommodation-preferences/:taskId",
            component: AccommodationPreferences,
          },
          {
            path: ":orderId/:variationId/payment/:taskId",
            component: CourseDetail,
          },
          {
            path: ":orderId/:variationId/payment-deposit/:taskId",
            component: CourseDetail,
          },
          {
            path: ":orderId/:productId/:variationId/course-detail",
            component: CourseDetail,
          },
          {
            path: ":orderId/:variationId/optional-extras",
            component: OptionalExtras,
          },
          {
            path: ":orderId/:productId/:variationId/payment-details/:paymentTaskId/:paymentDepositTaskId",
            component: PaymentDetails,
          },
        ],
      },
      {
        path: "parent-details",
        component: ParentDetails,
      },
      {
        path: "account-details",
        component: AccountDetails,
      },
      {
        path: "past-courses",
        component: PastCourses,
        redirect: "/past-courses/overview",
        children: [
          {
            path: "overview",
            component: PastCoursesMain,
          },
          {
            path: ":orderId/:productId/:variationId/report-card",
            component: ReportCard,
          },
          {
            path: ":orderId/:productId/:variationId/english-assessment-report-card",
            component: ReportCard,
            props: { type: "english-assessment" },
          },
          {
            path: ":orderId/:productId/:variationId/past-course",
            component: PastCourse,
          },
        ],
      },
    ],
  },
];

const routes = [
  {
    base: "/booking/",
    path: "/",
    redirect: "/start",
    component: Booking,
    children: [
      {
        path: "start",
        component: BookingStart,
        beforeEnter(to, from, next) {
          sessionStorage.removeItem("bucksmoreCart");
          Vue.set(BookingFlowStore, "selectedCourses", []);
          Vue.set(BookingFlowStore, "value", null);
          Vue.set(BookingFlowStore, "choosenVariations", []);
          Vue.set(BookingFlowStore, "coursesToBook", []);
          Vue.set(BookingFlowStore, "order", {}),
          next();
        },
      },
      {
        path: "form",
        redirect: "/form/1",
        component: BookingForm,
        children: [
          {
            path: "1",
            name: 1,
            component: Step01,
            props: {
              current: 1,
            },
          },
          {
            path: "2",
            name: 2,
            component: Step02,
          },
          {
            path: "3",
            name: 3,
            component: Step03,
            beforeEnter(to, from, next) {
              if (window.wpApiSettings.currentUser !== "0") {
                AccountService.getParentDetailsRequest().then(response => {
                  const fields = response.data;
                  Vue.set(BookingFlowStore, "parentDetails", {
                    adressLine1: fields.acf.account_user_address_1,
                    adressLine2: fields.acf.account_user_address_2,
                    areas: {
                      area: fields.acf.account_user_phone.slice(0, 3),
                    },
                    cityTown: fields.acf.account_user_city,
                    contactNumber: fields.acf.account_user_phone.slice(3, fields.acf.account_user_phone.length),
                    country: {
                      country:  fields.acf.account_user_country,
                    },
                    firstName: fields.user_firstname,
                    lastName: fields.user_lastname,
                    postalCode: fields.acf.account_user_zip_code,
                    titles: {
                      title: fields.acf.account_user_title,
                    },
                  }); 

                  next("/payment");
                });
              } else {
                next();
              }
            },
          },
          {
            path: "4",
            name: 4,
            component: Step04,
            beforeEnter(to, from, next) {
              if (window.wpApiSettings.currentUser !== "0") {
                next("/payment");
              } else {
                next();
              }
            },
          },
        ],
      },
      {
        path: "payment",
        name: 5,
        component: Step05,
      },
      {
        path: "summary",
        name: 6,
        component: bookingSummary,
      },
    ],
  },
];

const isBookingView = document.getElementById("AppBooking");
const isMyAccountView = document.getElementById("AppAccount");

Vue.component("v-select", vSelect);
/*

I have temporarily commented that one out because it is giving huge issues
Once we make the cart leaner we can put that back in but now it is breaking the browser and flow
*/
const BookingFlowStore = sessionStorage.bucksmoreCart
  ? JSON.parse(sessionStorage.bucksmoreCart)
  : {
      start: true,
      savedCourse: null,
      value: null,
      isWorking: false,
      courses_api_url: `${window.wpApiSettings.root}custom/v1/courses`,
      selectedCourses: [],
      courses: [],
      choosenVariations: [],
      coursesToBook: [],
      products: [],
      bookSingle: null,
      bookingdata: {},
      studentDetails: {
        firstName: null,
        lastName: null,
        gender: null,
        nationality: null,
        dateOfBirth: {
          day: null,
          month: null,
          year: null,
        },
      },
      parentDetails: {
        titles: null,
        firstName: null,
        lastName: null,
        adressLine1: null,
        adressLine2: null,
        cityTown: null,
        postalCode: null,
        country: null,
        areas: null,
        contactNumber: null,
      },
      order: {},
    };
/*

const BookingFlowStore = {
  start: true,
  savedCourse: null,
  value: null,
  isWorking: false,
  courses_api_url: `${window.wpApiSettings.root}custom/v1/courses`,
  selectedCourses: [],
  courses: [],
  choosenVariations: [],
  coursesToBook: [],
  products: [],
  bookSingle: null,
  bookingdata: {},
  studentDetails: {
    firstName: null,
    lastName: null,
    gender: null,
    nationality: null,
    dateOfBirth: {
      day: null,
      month: null,
      year: null,
    },
  },
  parentDetails: {
    titles: null,
    firstName: null,
    lastName: null,
    adressLine1: null,
    adressLine2: null,
    cityTown: null,
    postalCode: null,
    country: null,
    areas: null,
    contactNumber: null,
  },
  order: {},
};
*/
const vueObject = {
  el: "#App",
  components: {
    OurCourses,
    OurSubjects,
    NewsListing,
    Booking,
    EnglishTest,
    LocationsListing,
    Testimonials,
    Account,
    BoxIntegration,
  },
  data: {
    store: BookingFlowStore,
  },
};



if (isBookingView || isMyAccountView) Vue.use(VueRouter);

if (isBookingView) {
  const router = new VueRouter({ routes });
  new Vue({ ...vueObject, router });
} else if (isMyAccountView) {
  const myAccountRouter = new VueRouter({ routes: myAccountRoutes });
  new Vue({ ...vueObject, router: myAccountRouter });
} else {
  new Vue({ ...vueObject });
}

new Vue({
  el: "#App2",
  components: {
    Filters,
  },
});

$(() => {
  new MenuActions();
  new Nav();
  new SideActions();
  new HomeSlider();
  new GlobalSettings();
  new VideoPlayer();
  new News();
  new Favourites();
  new Subjects();
  new Locations();
  new Courses();
  new RelatedPosts();
  new Gallery();
  new AvailableSubjects();
  new Accordion();
  new Tutors();
  new TestimonialsCarousel();
  new singlePost();
});
