import { TweenMax, Power3 } from "gsap/all";
import $ from "jquery";
import _ from "lodash";

class sideActions {
  constructor() {
    this.toggleSideAction();
    this.resizeHandler();
    this.applyNowAction();
  }

  toggleSideAction() {
    const buttons = $(".sideActionBtn");
    const boxes = $(".sideAction");

    buttons.click(function() {
      const link = $(this).data("link");

      if (link) {
        window.location.replace(link);
        return;
      }

      const i = $(this).index(".sideActionBtn");
      let pageWidth = $(window).width();

      $(boxes[i]).addClass("active");
      TweenMax.to(".sideAction", 1, { x: 0, ease: Power3.easeInOut });
      if (pageWidth > 769) {
        TweenMax.to(boxes[i], 1, { x: -565, ease: Power3.easeInOut });
      } else {
        TweenMax.to(boxes[i], 1, {
          x: "-100vw",
          ease: Power3.easeInOut,
        });
      }
      TweenMax.to(".Overlay", 1, {
        zIndex: 10,
        opacity: 0.6,
        ease: Power3.easeInOut,
      });

      $(".sideActionBtn.current").not($(this)).removeClass("current");
      $(".sideAction.active").not($(boxes[i])).removeClass("active");

      if ($(this).hasClass("current")) {
        $(".Header__actions .sideActionBtn.current").removeClass("current");
        $(boxes[i]).removeClass("active");

        TweenMax.to(".sideAction", 1, { x: 0, ease: Power3.easeInOut });
        TweenMax.to(".Overlay", 1, {
          zIndex: -1,
          opacity: 0,
          ease: Power3.easeInOut,
        });
      } else {
        $(this).addClass("current");
      }
    });
  }

  resizeHandler() {
    $(window).on("orientationchange, resize", _.debounce(function() {
      const pageWidth = $(window).width();
      if (pageWidth > 769) {
        TweenMax.to(".sideAction.active", 1, {
          x: -565,
          ease: Power3.easeInOut,
        });
      } else {
        TweenMax.to(".sideAction.active", 1, {
          x: "-100vw",
          ease: Power3.easeInOut,
        });
      }
    }, 200));
  }

  applyNowAction() {
    $(".Header__actions__apply").click(function() {
      window.location.replace($(this).data("url"));
    });
  }
}

export default sideActions;
