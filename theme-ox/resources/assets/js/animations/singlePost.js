import { TweenMax, Power3 } from "gsap/all";
import $ from "jquery";

class singlePost {
  constructor() {
    this.toggleSidePost();
    this.resizeHandler();
  }

  toggleSidePost() {
    // $(".News").click(function() {
    //   TweenMax.to(".SingleNews", 1, { right:0, ease: Power3.easeInOut });
    // });
  }

  resizeHandler() {
    $(window).on("resize", function() {
      this.toggleSidePost;
    });

    $(window).on("orientationchange", function() {
      let pageWidth = $(window).width();

      setTimeout(function() {
        if (pageWidth > 769) {
          TweenMax.to(".sideAction.active", 1, {
            x: -565,
            ease: Power3.easeInOut,
          });
        } else {
          TweenMax.to(".sideAction.active", 1, {
            x: "-100vw",
            ease: Power3.easeInOut,
          });
        }
      }, 200);
    });
  }
}

export default singlePost;
