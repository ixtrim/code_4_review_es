import { TweenLite, Power3, TweenMax } from "gsap/all";
import $ from "jquery";
import _ from "lodash";

class Nav {
  constructor() {
    this.toggleSideNav();
    this.resizeNav();
    this.triggerPureChat();
    this.addMenuOverlay();
  }

  // cause now static method need to call not this.toggleSideNav() but Nav.toogleSideNav
  toggleSideNav() {
    const sideNav = $(".sideNav");
    const sideContact = $(".sideContact");
    const sideContactMobile = $(".sideNav__contactMobile");
    let xcontactTarget;
    let xNav;
    let xContact;
    let pageWidth = $(window).width();

    $(window).on("orientationchange", function() {
      setTimeout(function() {
        pageWidth = $(window).width();
      }, 100);
    });

    if (pageWidth > 1024) {
      xNav = "27vw";
      xContact = "54vw";
      xcontactTarget = sideContact;
    } else {
      xNav = "100vw";
      xContact = "100vw";
      xcontactTarget = sideContactMobile;
    }

    const sideNavAnimation = TweenLite.to(sideNav, 1, {
      x: xNav,
      ease: Power3.easeInOut,
    }).reverse();

    const sideContactAnimation = TweenLite.to(xcontactTarget, 1, {
      x: xContact,
      ease: Power3.easeInOut,
    }).reverse();

    $(".Header__toggler").on("click", function() {
      if (sideNavAnimation.reversed() || sideContactAnimation.reversed()) {
        sideNavAnimation.play();
        sideContactAnimation.play();
        TweenMax.to(".Overlay", 1, {
          zIndex: 10,
          opacity: 0.6,
          ease: Power3.easeInOut,
        });
        $(this).addClass("active");
        $(sideNav).addClass("active");
        $(sideContact).addClass("active");
        $(sideContactMobile).addClass("active");
      } else {
        sideNavAnimation.reverse();
        sideContactAnimation.reverse();
        TweenMax.to(".Overlay", 1, {
          zIndex: -1,
          opacity: 0,
          ease: Power3.easeInOut,
        });
        $(this).removeClass("active");
        $(sideNav).removeClass("active");
        $(sideContact).removeClass("active");
        $(sideContactMobile).removeClass("active");
      }
    });
}

addMenuOverlay() {
  $(".Header__nav").on("mouseover", function() {
    TweenMax.to(".Overlay", 1, {
      zIndex: 10,
      opacity: 1,
      ease: Power3.easeInOut,
      background: "#0f2245",
    });
  });

  $(".Header__nav").on("mouseleave", function() {
    TweenMax.to(".Overlay", 1, {
      zIndex: -1,
      opacity: 0,
      ease: Power3.easeInOut,
      background: "#0f2245",
    });
  });
}

  resizeNav() {
    $(window).on("orientationchange, resize", _.debounce(function() {
      // can be const because not reassinged
      // in fact should be this.pageWidth, class prop but not now
      const pageWidth = $(window).width();
      if ($(".sideNav").hasClass("active")) {
        setTimeout(function() {
          if (pageWidth > 1024) {
            TweenMax.to(".sideNav.active", 1, {
              x: "27vw",
              ease: Power3.easeInOut,
            });
            TweenMax.to(".sideContact.active", 1, {
              x: "54vw",
              ease: Power3.easeInOut,
            });
          } else {
            TweenMax.to(".sideNav.active,.sideNav__contactMobile.active", 1, {
              x: "100vw",
              ease: Power3.easeInOut,
            });
            TweenMax.to(".sideContact.active", 1, {
              x: "0",
              ease: Power3.easeInOut,
            });
          }
        }, 50);
      }
    }, 100));
  }

  triggerPureChat() {
    const button = $(".trigger-live-chat");

    if (button !== null) {
      $(button).click(function(e) {
        e.preventDefault();
        /* eslint-disable-next-line */
        purechatApi.set("chatbox.expanded", true); // Expanded the chat box (false collapses it)
      });
    }
  }
}

export default Nav;
