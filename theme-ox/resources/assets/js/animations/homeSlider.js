import { TweenMax, Power3, Power0, TimelineLite } from "gsap/all";
import $ from "jquery";
import Utils from "../utils/";

class HomeSlider {
  constructor() {
    this.blueOverlay();
    this.leftSlideContent();
    this.changeContent();
    this.coursesContent();
    this.subjectsContent();
    this.locationsContent();
    this.newsContent();
    this.lines();
    this.scroll();
  }

  scroll() {
    let pageWidth = $(window).width();
    let current = 1;

    function scrollNav(event) {
      if (pageWidth > 1024) {
        const delta = Math.sign(event.deltaY);
        let next = current;

        if (delta > 0) {
          next += 1;
        } else {
          next -= 1;
        }

        next = next > 5 ? 1 : next;
        next = next < 0 ? 5 : next;

        current = next;
        $("#slideBtn" + current).click();
        return false;
      }
    }

    window.addEventListener("wheel", Utils.debounce(scrollNav, 500, true));

    $(".sideActionBtn, .Header__toggler").click(function() {
      if ($(this).hasClass("current") || $(".Header__toggler").hasClass("active")) {
        window.removeEventListener("wheel", Utils.debounce(scrollNav, 50, true));
      } else {
        window.addEventListener("wheel", Utils.debounce(scrollNav, 50, true));
      }
    });
  }

  blueOverlay() {
    let pageWidth = $(window).width();

    if (pageWidth > 1024) {
      const rightSideWide = new TimelineLite({ paused: true });
      const rightSideNarrow = new TimelineLite({ paused: true });
      let blueWidth = "650";

      TweenMax.fromTo(
        ".Home__overlayBlue",
        1.2,
        { zIndex: 3, width: "100vw", ease: Power3.easeInOut, delay: 1.4 },
        { width: "50vw" }
      );

      TweenMax.fromTo(
        ".Home__overlayBlue__lines",
        2,
        {
          autoAlpha: 0,
          bottom: "-90%",
          left: "-25%",
          ease: Power3.easeInOut,
          delay: 1.4,
        },
        { bottom: "-75%", autoAlpha: 0.2 }
      );

      if (pageWidth > 1600) {
        blueWidth = "650";
      } else {
        blueWidth = "500";
      }

      rightSideWide.fromTo(
        ".Home__overlayBlue",
        1.6,
        { zIndex: 3, width: "50vw", ease: Power0.easeInOut },
        { zIndex: 3, width: blueWidth }
      );
      rightSideNarrow.fromTo(
        ".Home__slider__item--default .Home__slider__item__left ",
        3.4,
        { width: blueWidth, ease: Power0.easeInOut },
        { width: "50vw" }
      );

      $(".slideBtn").click(function() {
        if ($(".Home__slider__item--defalut").hasClass("current")) {
          rightSideWide.restart();
        }
      });

      $(".HomeDefault__btn.slideBtn").click(function() {
        TweenMax.fromTo(
          ".Home__overlayBlue",
          2,
          { width: "100vw", ease: Power3.easeInOut, delay: 1 },
          { width: "50vw" }
        );
      });
    }
  }

  lines() {
    const linesWelcome = new TimelineLite({ paused: true });
    const linesCourses = new TimelineLite({ paused: true });
    const linesSubjects = new TimelineLite({ paused: true });
    const linesLocations = new TimelineLite({ paused: true });
    const linesNews = new TimelineLite({ paused: true });

    linesWelcome.fromTo(
      ".Home__overlayBlue__lines",
      2,
      { bottom: "-90%", left: "-25%", ease: Power3.easeInOut, delay: 1.4 },
      { bottom: "-75%" }
    );

    linesCourses.fromTo(
      ".Home__overlayBlue__lines",
      2,
      { bottom: "-75%", left: "-25%", ease: Power3.easeInOut, delay: 1.4 },
      { bottom: "-60%", left: "-70%" }
    );

    linesSubjects.fromTo(
      ".Home__overlayBlue__lines",
      2,
      { bottom: "-60%", left: "-70%", ease: Power3.easeInOut, delay: 1.4 },
      { bottom: "-35%", left: "-100%" }
    );

    linesLocations.fromTo(
      ".Home__overlayBlue__lines",
      2,
      { bottom: "-35%", left: "-100%", ease: Power3.easeInOut, delay: 1.4 },
      { bottom: "-20%", left: "-145%" }
    );

    linesNews.fromTo(
      ".Home__overlayBlue__lines",
      2,
      { bottom: "-20%", left: "-145%", ease: Power3.easeInOut, delay: 1.4 },
      { bottom: "-10%", left: "-150%" }
    );

    $(".HomeDefault__btn.slideBtn").click(function() {
      linesWelcome.restart();
    });

    $(".HomeCourses__btn.slideBtn").click(function() {
      linesCourses.restart();
    });

    $(".HomeSubjects__btn.slideBtn").click(function() {
      linesSubjects.restart();
    });

    $(".HomeLocations__btn.slideBtn").click(function() {
      linesLocations.restart();
    });

    $(".HomeNews__btn.slideBtn").click(function() {
      linesNews.restart();
    });
  }

  leftSlideContent() {
    const tl = new TimelineLite();

    tl.from(
      ".Home__slider__item__left .Home__slider__item__left__inner__subtitle",
      1,
      { autoAlpha: 0, ease: Power3.easeIn, delay: 0.3 },
      0
    )
      .from(
        ".Home__slider__item__left .Home__slider__item__left__inner__title",
        1,
        { autoAlpha: 0, ease: Power3.easeIn, delay: 0.3 },
        0
      )
      .from(
        ".Home__slider__item__left .Home__slider__item__left__inner__text",
        1,
        { autoAlpha: 0, ease: Power3.easeIn, delay: 0.3 },
        0
      )
      .from(
        ".Home__slider__item__left .button",
        1,
        { autoAlpha: 0, ease: Power3.easeIn, delay: 0.3 },
        0
      );

    $(".slideBtn").click(function() {
      tl.restart();
    });
  }

  coursesContent() {
    const tlCourse = new TimelineLite();

    tlCourse
      .staggerFrom(
        ".HomeCourses__single ",
        3,
        { x: 500, autoAlpha: 0, delay: 0, ease: Power3.easeOut },
        0.2
      )
      .to(".HomeCourses .CarouselPagination", 1.2, {
        opacity: 1,
        delay: -2.5,
        ease: Power3.easeInOut,
      });

    $(".HomeCourses__btn.slideBtn").click(function() {
      tlCourse.restart();
    });

    $(".swiper-course-next,.swiper-course-prev").click(function() {
      setTimeout(function() {
        //maybe needed
      }, 2);
    });
  }

  subjectsContent() {
    $(".HomeSubjects__btn.slideBtn").click(function() {
      TweenMax.fromTo(
        ".Home__slider__item__right__subjects",
        3,
        { autoAlpha: 0, ease: Power3.easeInOut, delay: 0.5 },
        { autoAlpha: 1, delay: 0.5 }
      );
      TweenMax.to(".HomeSubjects .CarouselPagination", 1.2, {
        autoAlpha: 1,
        delay: 1,
        ease: Power3.easeInOut,
      });
    });
  }

  locationsContent() {
    $(".HomeLocations__btn.slideBtn").click(function() {
      TweenMax.fromTo(
        ".Home__slider__item__right__locations",
        3,
        { autoAlpha: 0, ease: Power3.easeInOut, delay: 0.5 },
        { autoAlpha: 1, delay: 0.5 }
      );
      TweenMax.to(".HomeLocations .CarouselPagination", 1.2, {
        autoAlpha: 1,
        delay: 1,
        ease: Power3.easeInOut,
      });
    });
  }

  newsContent() {
    $(".HomeNews__btn.slideBtn").click(function() {
      TweenMax.fromTo(
        ".HomeNews__single__image",
        0.9,
        { ease: Power3.easeInOut, scaleX: 0, transformOrigin: "left" },
        { scaleX: 1, transformOrigin: "right", delay: 0.5 }
      );
      TweenMax.fromTo(
        ".HomeNews__single__content",
        0.9,
        { ease: Power3.easeInOut, scaleX: 0, transformOrigin: "right" },
        { scaleX: 1, transformOrigin: "left", delay: 0.5 }
      );
      TweenMax.to(".HomeNews .CarouselPagination", 1.2, {
        autoAlpha: 1,
        delay: 1,
        ease: Power3.easeInOut,
      });
    });
  }

  changeContent() {
    const buttons = $(".slideBtn");
    const boxes = $(".homeSlide");

    $(".slideBtn").first().addClass("current");
    $(".Home__slider__item").first().addClass("current");

    buttons.click(function() {
      const i = $(this).index(".slideBtn");

      $(".slideBtn.current, .Home__slider__item.current").not($(this)).removeClass("current");

      $(this).addClass("current");
      $(boxes[i]).addClass("current");
    });
  }
}

export default HomeSlider;
