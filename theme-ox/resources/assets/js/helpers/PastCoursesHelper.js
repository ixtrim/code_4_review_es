export class PastCoursesHelper {
 /** 
  * Returns an array of the courses grouped by year
  * @param {array} courses
  * @returns {object} [{2018: [], 2019: []}]
  */
  static groupCoursesByYear(courses) {
    const coursesByYear = {};
    courses.forEach(course => {
      const year = course.dates.startDate.year;
      if (coursesByYear[year]) {
        coursesByYear[year].push(course);
      } else {
        coursesByYear[year] = [course];
      }
    });
    console.log("coursesByYear", coursesByYear);
    return coursesByYear;
  }
}

export default PastCoursesHelper;