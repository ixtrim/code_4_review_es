import Swiper from "swiper/dist/js/swiper.js";

class Tutors {
  constructor() {
    this.Tutors();
  }

  Tutors() {
    this.mySwiper = new Swiper(".Tutors__swiper", {
      speed: 800,
      slidesPerView: 2.1,
      spaceBetween: 100,
      centeredSlides: true,
      loop: true,
      navigation: {
        nextEl: ".swiper-tutors-next",
        prevEl: ".swiper-tutors-prev",
      },
      pagination: {
        el: ".Tutors__counter",
        type: "custom",
        clickable: true,
        renderCustom: function(swiper, current, total) {
          function numberAppend(d) {
            return d < 10 ? "" + d.toString() : d.toString();
          }
          return ("<span>" + numberAppend(current) + "</span>" + " <span>/</span> " + numberAppend(total));
        },
      },
      observer: true,
      observeParents: true,
      breakpoints: {
        768: {
          spaceBetween: 20,
          slidesPerView: 1,
        },
        1200: {
          slidesPerView: 1.8,
          spaceBetween: 100,
        },
        1440: {
          slidesPerView: 2.1,
          spaceBetween: 80,
        },
      },
    });
  }
}

export default Tutors;
