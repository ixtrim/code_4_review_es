import $ from "jquery";

class VideoPlayer {
  constructor() {
    this.videoPlayer();
  }

  videoPlayer() {
    $("video").on("webkitendfullscreen", function() {
      $(".modal").removeClass("is-active");
      $(".modal_content").html("");
    });

    $(".image_button").on("click", function(event) {
      event.stopPropagation();

      const video = $(this).next(".video").find("video").find("Source:first").attr("src");
      const youtubeId = $(this).next(".video").find("button").data("youtubeId");
      let content;

      if (youtubeId) {
        content = `<iframe width="560" height="315" src="https://www.youtube.com/embed/${youtubeId}?autoplay=1&showinfo=0&showsearch=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
        $(".modal_content,.modal_container").addClass("is-youtube");
      } else {
        content = `<video controls autoplay><source src="${video}" type="video/mp4"></video>`;
      }

      $(".modal").addClass("is-active");
      $(".modal_content").html(content);
    });

    $("body").on("click", ".js-video_button",function(event) {
      event.stopPropagation();

      const video = $(this).siblings("video").find("Source:first").attr("src");
      const youtubeId = $(this).data("youtubeId");
      let content;

      if (youtubeId) {
        content = `<iframe width="560" height="315" src="https://www.youtube.com/embed/${youtubeId}?autoplay=1&showinfo=0&showsearch=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
        $(".modal_content,.modal_container").addClass("is-youtube");
      } else {
        content = `<video controls autoplay><source src="${video}" type="video/mp4"></video>`;
      }

      $(".modal").addClass("is-active");
      $(".modal_content").html(content);
    });

    $(".modal").on("click", function() {
      $(this).removeClass("is-active");
      $(".modal_content,.modal_container").html("").removeClass("is-youtube");
    });

    $(".modal_exit").on("click", function(element) {
      element.stopPropagation();
      $(".modal").removeClass("is-active");
      $(".modal_content").html("").removeClass("is-youtube");
    });

    $(".modal_container").on("click", function(event) {
      event.stopPropagation();
    });
  }
}

export default VideoPlayer;
