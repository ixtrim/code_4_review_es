import Swiper from "swiper/dist/js/swiper.js";
import $ from "jquery";
import "magnific-popup";

class Gallery {
  constructor() {
    this.Gallery();
  }

  Gallery() {
    this.mySwiper = new Swiper(".Gallery__swiper", {
      speed: 500,
      slidesPerView: 1,
      effect: "fade",
      navigation: {
        nextEl: ".swiper-gallery-next",
        prevEl: ".swiper-gallery-prev",
      },
      fadeEffect: {
        crossFade: true,
      },
      observer: true,
      observeParents: true,
      pagination: {
        el: ".Gallery__counter",
        type: "custom",
        clickable: true,
        renderCustom: function(swiper, current, total) {
          function numberAppend(d) {
            return d < 10 ? "" + d.toString() : d.toString();
          }
          return ("<span>" + numberAppend(current) + "</span>" + " <span>/</span>" + numberAppend(total));
        },
      },
    });

    $(".activeGallery").closest(".Gallery").addClass("is-active");

    $(".popup-gallery").magnificPopup({
      delegate: "a",
      type: "image",
      tLoading: "Loading image #%curr%...",
      mainClass: "mfp-img-mobile",
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1],
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow custom-arrow mfp-arrow-%dir%"></button>', // eslint-disable-line
        tPrev: "Previous",
        tNext: "Next",
      },
      closeMarkup: '<span class="mfp-close"><img class="mfp-close" src="/wp-content/themes/bucksmore/resources/assets/images/expand.png" width="65" height="65" /></span>', // eslint-disable-line
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.', // eslint-disable-line
        titleSrc: function(item) {
          return item.el.attr("title");
        },
      },
      callbacks: {
        buildControls: function() {
          // re-appends controls inside the main container
          this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
        },
      },
    });

    $("a.btn-gallery").on("click", function(event) {
      event.preventDefault();

      let gallery = $(this).attr("href");

      $(gallery).magnificPopup({
        delegate: "a",
        type: "image",
        tLoading: "Loading image #%curr%...",
        mainClass: "mfp-img-mobile",
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0, 1],
          arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow custom-arrow mfp-arrow-%dir%"></button>', // eslint-disable-line
          tPrev: "Previous",
          tNext: "Next",
        },
        closeMarkup: '<span class="mfp-close"><img class="mfp-close" src="/wp-content/themes/bucksmore/resources/assets/images/expand.png" width="65" height="65" /></span>', // eslint-disable-line
        image: {
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.', // eslint-disable-line
          titleSrc: function(item) {
            return item.el.attr("caption");
          },
        },
        callbacks: {
          buildControls: function() {
            // re-appends controls inside the main container
            this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
          },
        },
      })
      .magnificPopup("open");
    });
  }
}

export default Gallery;
