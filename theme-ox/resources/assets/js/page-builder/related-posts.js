import Swiper from "swiper/dist/js/swiper.js";

class RelatedPosts {
  constructor() {
    this.locationsCarousel();
  }

  locationsCarousel() {
    this.mySwiper = new Swiper(".RelatedPosts__location--slider", {
      speed: 500,
      slidesPerView: 1,
      effect: "fade",
      navigation: {
        nextEl: ".swiper-location-course-next",
        prevEl: ".swiper-location-course-prev",
      },
      observer: true,
      observeParents: true,
    });
  }
}

export default RelatedPosts;
