import $ from "jquery";

class AvialableSubjects {
  constructor() {
    this.switchContent();
    this.switchCourseContent();
  }

  switchContent() {
    $(".AvailableSubjects__tabs__single:nth-child(1),.AvailableSubjects__accordions:nth-child(1)").addClass("current");
    $(".AvailableSubjects__tabs__single").click(function() {
      const el = $(this),
        target = el.data("target");
      $(target).addClass("current").siblings().removeClass("current");
      $(this).addClass("current").siblings().removeClass("current");
    });
  }

  switchCourseContent() {
    $(".SingleCourse__menu__list a").first().addClass("current");
    $(".SingleCourse__content .component.menu1").addClass("current");
    $(".SingleCourse__menu__list a").on("click", function() {
      const el = $(this),
        target = el.data("target");
      $(target).addClass("current");
      $(".component.current").not($(target)).removeClass("current");
      $(this).addClass("current").siblings().removeClass("current");
    });
  }
}

export default AvialableSubjects;
