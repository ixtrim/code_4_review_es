import Swiper from "swiper/dist/js/swiper.js";
import $ from "jquery";

class Locations {
  constructor() {
    this.locationsCarousel();
    this.locationHover();
  }

  locationsCarousel() {
    this.mySwiper = new Swiper(".HomeLocations__carousel__init", {
      speed: 1000,
      slidesPerView: "auto",
      slidesPerColumn: 2,
      slidesPerGroup: 1,
      spaceBetween: 25,
      navigation: {
        nextEl: ".swiper-location-next",
        prevEl: ".swiper-location-prev",
      },
      breakpoints: {
        1920: {
          slidesPerView: "auto",
          slidesPerColumn: 2,
        },
        1024: {
          spaceBetween: 10,
        },
        680: {
          slidesPerView: 2.4,
          slidesPerColumn: 1,
          spaceBetween: 25,
        },
        640: {
          slidesPerView: 2.2,
          slidesPerColumn: 1,
        },
        500: {
          slidesPerView: 1.6,
          slidesPerColumn: 1,
        },
        400: {
          slidesPerView: 1.4,
          slidesPerColumn: 1,
        },
      },
    });
  }

  locationHover() {
    $(".HomeLocations__single").hover(function() {
      $(".HomeLocations__single").toggleClass("active");
    });
  }
}

export default Locations;
