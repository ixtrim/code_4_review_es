import Swiper from "swiper/dist/js/swiper.js";

class Subjects {
  constructor() {
    this.subjectsCarousel();
  }

  subjectsCarousel() {
    this.mySwiper = new Swiper(".HomeSubjects__carousel__init", {
      speed: 1000,
      slidesPerColumn: 2,
      slidesPerGroup: 3,
      slidesPerView: 4,
      spaceBetween: 25,
      navigation: {
        nextEl: ".swiper-subject-next",
        prevEl: ".swiper-subject-prev",
      },
      breakpoints: {
        1920: {
          slidesPerColumn: 2,
          slidesPerGroup: 3,
          slidesPerView: 4,
          spaceBetween: 45,
        },
        1660: {
          slidesPerColumn: 2,
          slidesPerGroup: 3,
          slidesPerView: 3,
          spaceBetween: 25,
        },
        1365: {
          slidesPerColumn: 2,
          slidesPerGroup: 2,
          slidesPerView: 2,
        },
        1200: {
          slidesPerColumn: 2,
          slidesPerGroup: 1,
          slidesPerView: 1,
        },
        1024: {
          slidesPerColumn: 2,
          slidesPerGroup: 3,
          slidesPerView: 3,
        },
        767: {
          slidesPerView: 1.8,
        },
        640: {
          slidesPerView: 1.5,
        },
        400: {
          slidesPerGroup: 1.3,
          slidesPerColumn: 1,
          slidesPerView: 1.3,
        },
      },
    });
  }
}

export default Subjects;
