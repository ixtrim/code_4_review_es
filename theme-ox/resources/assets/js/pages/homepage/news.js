import Swiper from "swiper/dist/js/swiper.js";

class News {
  constructor() {
    this.newsCarousel();
  }

  newsCarousel() {
    this.mySwiper = new Swiper(".HomeNews__carousel__init", {
      speed: 1000,
      loop: true,
      slidesPerView: 1.4,
      calculateHeight: true,
      navigation: {
        nextEl: ".swiper-news-next",
        prevEl: ".swiper-news-prev",
      },
      observer: true,
      observeParents: true,
      breakpoints: {
        2200: {
          slidesPerView: 1.6,
          spaceBetween: 40,
        },
        1750: {
          slidesPerView: 1.4,
          spaceBetween: 40,
        },
        1500: {
          slidesPerView: 1.3,
          spaceBetween: 40,
        },
        1365: {
          slidesPerView: 1.35,
          spaceBetween: 40,
        },
        1200: {
          slidesPerView: 1,
          spaceBetween: 80,
        },
        1024: {
          slidesPerView: 1.2,
          spaceBetween: 40,
        },
        768: {
          slidesPerView: 1.1,
          spaceBetween: 30,
        },
        680: {
          slidesPerView: 1.3,
          spaceBetween: 30,
        },
        500: {
          slidesPerView: 1,
          spaceBetween: 0,
        },
      },
    });
  }
}

export default News;
