import $ from "jquery";
import axios from "axios";
import Swiper from "swiper/dist/js/swiper.js";
import PerfectScrollbar from "perfect-scrollbar";

class Courses {
  constructor() {
    this.coursesCarousel();
    this.showSingleLocation();
    this.hideSingleLocation();
    this.markActiveMenu();
  }

  markActiveMenu() {
    const container = document.getElementsByClassName("SingleCourse__content")[0];

    if(container !== undefined) {

    const menu1 = $(".menu1:first");
    const menu2 = $(".menu2:first");
    const menu3 = $(".menu3:first");
    const menu4 = $(".menu4:first");
    const menu5 = $(".menu5:first");
    const menu6 = $(".menu6:first");    

    container.addEventListener("ps-scroll-y", (e) => {
        const scrolled = e.target.scrollTop;
        // this is an overflowed div so bear with me, all these calculations are necessary
        // also it must match click event
        // this should be done in more elegant way in loop but no time now.
        const result1 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu1).offset().top - $(".SingleCourse__content").offset().top));
        const result2 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu2).offset().top - $(".SingleCourse__content").offset().top));
        const result3 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu3).offset().top - $(".SingleCourse__content").offset().top));
        const result4 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu4).offset().top - $(".SingleCourse__content").offset().top));
        const result5 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu5).offset().top - $(".SingleCourse__content").offset().top));
        const result6 = Math.floor($(".SingleCourse__content").scrollTop() + ($(menu6).offset().top - $(".SingleCourse__content").offset().top));

        if (scrolled >= result1 && scrolled < result2) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu1").addClass("current");
        }
        if (scrolled >= result2 && scrolled < result3) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu2").addClass("current");
        }
        if (scrolled >= result3 && scrolled < result4) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu3").addClass("current");
        }
        if (scrolled >= result4 && scrolled < result5) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu4").addClass("current");
        }
        if (scrolled >= result5 && scrolled < result6) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu5").addClass("current");
        }
        if (scrolled >= result6) {
          $(".menu-item").removeClass("current");
          $(".menu-item-menu6").addClass("current");
        }
      });
    }
  }

  showSingleLocation() {
    $(".getLocation").on("click", async function() {
      const postId = $(this).data("id");
      try{
      $(this).addClass("isLoading");
      $(this).text("Loading");
      const response = await axios.get(`${window.wpApiSettings.root}custom/v1/locations/${postId}/full`);

      const locationPageHtml = response.data.locationPage;
      $("#LocationContainer").html(locationPageHtml);
      $("#LocationContainer").addClass("LocationSlideout__extended");
      const isAndroid = /(android)/i.test(navigator.userAgent);
      if(!isAndroid){
        if($(".custom-scrollbar").length > 0) {
          $(".custom-scrollbar").each(function() { 
            new PerfectScrollbar($(this)[0], {
              minScrollbarLength: 200,
              maxScrollbarLength: 200,
              suppressScrollX: true });
          });
        }
      }
      }
      catch(e) {
        console.log(e);
      }
      finally {
        $(this).removeClass("isLoading");
        $(this).text("Learn More");
      }
    });
  }

  hideSingleLocation() {
    $("body").on("click", ".SingleLocation__close", function() {
      $("#LocationContainer").removeClass("LocationSlideout__extended");
    });
  }

  coursesCarousel() {
    this.mySwiper = new Swiper(".HomeCourses__carousel__init", {
      speed: 1000,
      slidesPerView: "4",
      spaceBetween: 0,
      calculateHeight: true,
      navigation: {
        nextEl: ".swiper-course-next",
        prevEl: ".swiper-course-prev",
      },
      breakpoints: {
        1920: {
          slidesPerView: "4",
          speed: 1500,
        },
        1760: {
          slidesPerView: "3",
          spaceBetween: 25,
        },
        1366: {
          slidesPerView: "3",
          spaceBetween: 10,
        },
        1365: {
          slidesPerView: "2",
          spaceBetween: 10,
        },
        1100: {
          slidesPerView: "1",
          spaceBetween: 10,
        },
        1023: {
          slidesPerView: "3",
          spaceBetween: 30,
        },
        900: {
          slidesPerView: "2.4",
          spaceBetween: 30,
        },
        767: {
          slidesPerView: "1.9",
          spaceBetween: 30,
        },
        680: {
          slidesPerView: "1.6",
          spaceBetween: 30,
        },
        480: {
          slidesPerView: "1.4",
          spaceBetween: 30,
        },
      },
    });
  }
}

export default Courses;
