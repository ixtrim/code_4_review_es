import $ from "jquery";
import Utils from "../../utils/";

class Favourites {
  constructor() {
    this.init();
  }

  init() {
    this.populateFavourites();
    this.handleDelete();
    this.checkFavourites();

    $(".SingleCourse__save").on("click touch", e => {
      const text = $(".favourite-text");
      const savedText = $(".favourite-text-saved");
      const feed = $(".SingleCourse__save").data("feed");
      const existingCookie = Utils.getCookie("Favourites") || "[]";
      const cookieContent = JSON.parse(existingCookie);
      const checkDuplicates = cookieContent.filter(element => {
        return element.id === feed.id && element;
      }).length;

      if (!checkDuplicates) {
        cookieContent.push(feed);
        $(e.currentTarget).addClass("saved");
        $(text).addClass("hidden");
        $(savedText).removeClass("hidden");
      }

      Utils.setCookie("Favourites", JSON.stringify(cookieContent), 100);
      this.populateFavourites();
    });
  }

  checkFavourites() {
    const container = $(".SingleCourse__save");
    if (container.length != 0) {
      const text = $(".favourite-text");
      const savedText = $(".favourite-text-saved");
      const feed = $(".SingleCourse__save").data("feed");
      const existingCookie = Utils.getCookie("Favourites") || "[]";
      const cookieContent = JSON.parse(existingCookie);
      const checkDuplicates = cookieContent.filter(element => {
        return element.id === feed.id && element;
      }).length;

      if (checkDuplicates) {
        $(container).addClass("saved");
        $(text).addClass("hidden");
        $(savedText).removeClass("hidden");
      }
    }
  }

  handleDelete() {
    $("body").on("click", ".sideAction__courses__content__element__delete", event => {
      const text = $(".favourite-text");
      const savedText = $(".favourite-text-saved");
      const id = $(event.currentTarget).data("id");
      const existingCookie = Utils.getCookie("Favourites") || "[]";
      const cookieContent = JSON.parse(existingCookie);
      const filteredCookie = cookieContent.filter(element => {
        return parseInt(element.id, 10) !== id;
      });


        $("#save-" + id).removeClass("saved");
        $(savedText).addClass("hidden");
        $(text).removeClass("hidden");


      Utils.setCookie("Favourites", JSON.stringify(filteredCookie), 100);
      this.populateFavourites();
    });
  }

  populateFavourites() {
    const favourites = JSON.parse(Utils.getCookie("Favourites")) || [];
    const amount = favourites.length;
    const content = $(".sideAction__courses__content");
    const holdingPage = window.location + "/holding-page";

    if (amount) {
      const svg = content.data("svg");

      $(".Header__actions__like").append(`<span class="Header__actions__like__amount" >${amount}</span>`);

      content.html("");

      favourites.forEach(element => {
        content.append(`
          <div class="sideAction__courses__content__element">
            <div class="sideAction__courses__content__element_age">${element.age} years</div>
            <h3 class="sideAction__courses__content__element__title">${element.title}</h3>
            <div class="sideAction__courses__content__element__location">${element.type}</div>
            <div class="sideAction__courses__content__element__button__group">
              <a href="${holdingPage}" class="button">Continue booking</a>
              <a class="button is-transparent" href="${element.link}" class="sideAction__courses__content__element__button">View course</a>
            </div>
            <div class="sideAction__courses__content__element__delete" data-id="${element.id}">
              <span class="icon SvgContainer" aria-hidden="true">
                ${svg}
              </span>
            </div>
          </div>
        `);
      });
    } else {
      content.html("");
      $(".Header__actions__like__amount").remove();
    }
  }
}

export default Favourites;
