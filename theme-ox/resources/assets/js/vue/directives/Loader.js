const LOADER_CLASS = "Loader";
const IS_LOADING_CLASS = "Loader--isLoading";

export const Loader = (el, { value }) => {
  el.classList.add(LOADER_CLASS);
  if (value) {
    el.classList.add(IS_LOADING_CLASS);
  } else {
    el.classList.remove(IS_LOADING_CLASS);
  }
};

export default Loader;