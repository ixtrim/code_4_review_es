import { axiosCustomWpApi } from "./axios";

class EnglishTestApi {
    static getQuestionsRequest(url) {
        return axiosCustomWpApi.get(`${url}questions`);
    }
    static sendEnglishTestDataRequest(url,data) {
        return axiosCustomWpApi.post(`${url}results`, data);
    }
}

export default EnglishTestApi;
