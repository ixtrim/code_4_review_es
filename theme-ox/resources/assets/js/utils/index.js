import $ from "jquery";
const bodyScrollLock = require("body-scroll-lock"); //eslint-disable-line

class Utils {
    static getViewportHeight() {
        return Math.min(
            window.innerHeight || Infinity,
            window.screen.innerHeight || Infinity,
            window.height || Infinity,
            window.screen.availHeight || Infinity
        );
    }

    static getViewportWidth() {
        return Math.min(
            window.innerWidth || Infinity,
            window.screen.innerWidth || Infinity,
            window.width || Infinity,
            window.screen.availWidth || Infinity
        );
    }

    static uuid(length = 8) {
        const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz".split(
            ""
        );

        if (!length) {
            length = Math.floor(Math.random() * chars.length);
        }

        let str = "";
        for (let i = 0; i < length; i++) {
            str += chars[Math.floor(Math.random() * chars.length)];
        }
        return str;
    }

    static getTranslation(key) {
        return window.__OMD_GLOBAL__.translations[key];
    }

    static getWaypointOffset() {
        return Utils.isMobile() ? "100%" : "80%";
    }

    static isMobile() {
        return $(window).width() < 768;
    }

    static lockBody($element) {
        bodyScrollLock.disableBodyScroll($element);
    }

    static unlockBody($element) {
        bodyScrollLock.enableBodyScroll($element);
    }

    static getUrlParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"); //eslint-disable-line
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results === null
            ? ""
            : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    static isAndroid() {
        return /(android)/i.test(navigator.userAgent);
    }

    static isIpad() {
        return navigator.userAgent.match(/iPad/i) != null;
    }

    static inView($item, callbackFunction, offset = "40%") {
        return $item.viewportChecker({
            offset,
            callbackFunction,
        });
    }

    /**
     * Left-pad a number with zeros so that it's at least the given
     * number of characters long
     * @param n   The number to pad
     * @param len The desired length
     */
    static leftPad(n, len = 2) {
        return new Array(len - String(n).length + 1).join("0").concat(n);
    }

    static debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    static setCookie(name, value, days) {
        var d = new Date();
        d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
        document.cookie =
            name + "=" + value + ";path=/;expires=" + d.toGMTString();
    }

    static getCookie(name) {
        var v = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
        return v ? v[2] : null;
    }

    /**
     * Changes the value of the specified key in the array to null
     * why? ACF Bug: https://support.advancedcustomfields.com/forums/topic/get_field-returning-array-for-select-when-no-value-exists-in-database/
     * @param keys {array} The keys to nullify
     * @param arr {array}  The array to operate on
     */
    static nullifyKeys(keys, arr) {
        keys.forEach(key => arr[key] && arr[key].length === 0 ? arr[key] = undefined : null);
        return arr;
    }
}

export default Utils;
