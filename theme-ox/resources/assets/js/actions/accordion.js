import $ from "jquery";

class Accordion {
  constructor() {
    if (!$(".Accordion").length) return;
    const self = this;
    $(document).on("click", ".Accordion__item__title", e => {
      self.toggle(e.target);
    });
  }

  toggle(target) {
    let $target = $(target);
    const $parent = $target.closest(".Accordion__item");
    const $item = $(".Accordion__item");
    const $container = $parent.find(".Accordion__item__content");
    const $itemContainer = $item.find(".Accordion__item__content");
    const $wrapper = $container.find(".Accordion__item__wrapper");
    const wrapperHeight = $wrapper.outerHeight();

    const openedClass = "Accordion__item--isOpen";
    if ($parent.hasClass(openedClass)) {
      $parent.removeClass(openedClass);
      $item.removeClass(openedClass);
      $container.css("height", 0);
    } else {
      $item.removeClass(openedClass);
      $parent.addClass(openedClass);
      $itemContainer.css("height", 0);
      $(".Accordion__item--isOpen .Accordion__item__content,.Accordion__item--isOpen .Accordion__item__wrapper").css("height", wrapperHeight);
    }
    const $window = $(window);

    /**
     *  Need this poopery to make sure that the parallax recalculates
     *  for each animation frame otherwise the parallax frames don't
     *  realise the page has changed.
     */
    const interval = setInterval(() => {
      $window.trigger("resize").trigger("scroll");
    }, 30);

    setTimeout(() => {
      clearInterval(interval);
    }, 400);
  }
}

export default Accordion;
