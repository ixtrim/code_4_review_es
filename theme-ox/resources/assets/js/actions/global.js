import PerfectScrollbar from "perfect-scrollbar";
import $ from "jquery";
import "select2";
import "select2/dist/css/select2.css";


class GlobalSettings {
  constructor() {
    this.showContent();
    this.expandedContent();
    this.switchClass();
    this.scrollToElement();
    this.aboutModal();
    this.customScrollBar();
    this.addSelect2();
  }

  showContent() {
    $("body").fadeIn(1300);
  }

  expandedContent() {
    const About = $(".About");
    $(".About__right__hero__expand").on("click", function() {
      $(About).addClass("expanded");
    });

    $(".About__right__close").on("click", function() {
      $(".About.expanded").removeClass("expanded");
    });
  }

  switchClass() {
    $(".radioBtn").on("click", function() {
      $(this).addClass("selected");
      $(".radioBtn")
        .not(this)
        .removeClass("selected");
    });
  }

  scrollToElement() {
    $(".SingleCourse__menu__list a").on("click touch", function() {
      const target = $(this).data("target");
      const className = `${target}:first`;
      if ($(className).length > 0) {
        const result = $(".SingleCourse__content").scrollTop() + ($(className).offset().top - $(".SingleCourse__content").offset().top);
        $(".SingleCourse__content").animate({ scrollTop: result }, 800);
      }
    });


    function scrollTop() {
      $("html, body").animate({
        scrollTop: $("#AppBooking").offset().top,
      }, 2000);
    }

    $("body").on("click", ".booking__button.--next,.booking__button.--prev", scrollTop);


  }

  aboutModal() {
    $(window).on("orientationchange load", function() {
      let pageWidth = $(window).width();
      const aboutContent = $(".About__right");
      if (pageWidth <= 1024) {
        aboutContent.addClass("is-modal");
      } else {
        aboutContent.removeClass("is-modal");
      }
    });

    $(".About__menu__item.is-active a:first").on("click", function(event) {
      event.preventDefault();
      $(".About__right").addClass("is-modal");
    });

    $(".About__right__close").on("click", function() {
      $(".About__right.is-modal").removeClass("is-modal");
    });
  }

  customScrollBar() {
    // todo - investigate why scrollbar not working on android devices
    const isAndroid = /(android)/i.test(navigator.userAgent);
    if(!isAndroid){
      if($(".custom-scrollbar").length > 0) {
        $(".custom-scrollbar").each(function() {
          new PerfectScrollbar($(this)[0], {
            minScrollbarLength: 200,
            maxScrollbarLength: 200,
            suppressScrollX: true });
        });
      }
    }
  }

  addSelect2() {
    if ($(".gfield_select").length > 0){
      $(".gfield_select").select2();
    }
  }

}

export default GlobalSettings;
