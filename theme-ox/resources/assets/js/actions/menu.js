import $ from "jquery";

class MenuActions {
  constructor() {
    this.$itemWithChild = $(".sideNav__item--hasChildren");
    this.$sideSubmenu = $(".sideNav__item__submenu");
    this.$aboutMenu = $(".About__menu__submenu__item");
    this.bindEvents();
    this.menuActive();
  }

  bindEvents() {
    this.$itemWithChild.find(".sideNav__item__link").on("click", this.toggleMe);
    this.$sideSubmenu.hide();
  }

  toggleMe(e) {
    e.preventDefault();
    $(this).find(".plus-minus-toggle").toggleClass("collapsed");
    $(this).next(".sideNav__item__submenu").slideToggle("500");
  }

  menuActive() {
    if ($(".About__menu__submenu__item").hasClass("is-active")) {
      $(".About__menu__submenu__item.is-active").closest(".About__menu__item--hasChildren").addClass("is-active");
    }
  }
}

export default MenuActions;
