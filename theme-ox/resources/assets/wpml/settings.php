<?php

remove_action('wp_head', [$sitepress, 'meta_generator_tag']);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

remove_action('load-update-core.php','wp_update_plugins');
add_filter('pre_site_transient_update_plugins','__return_null');

function icl_languages($separator='') {
    $languages = icl_get_languages('skip_missing=0');
    if (!empty($languages) && count($languages) > 1) : ?>
        <div class="langs">
            <span class="langs__current"><?php echo ICL_LANGUAGE_CODE ?></span>
            <?php foreach($languages as $l) : ?>
                <?php if ($l['active']) continue; ?>
                <a href="<?php echo $l['url'] ?>"><?php echo explode('-', $l['language_code'])[0] ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php } ?>
