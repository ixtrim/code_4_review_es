<?php

function get_template_messages()
{
    return [
        // 404 Page
        '404' => [
            'title'     => __('404 - Page not found.', 'bucksmore'),
            'body'      => __('The page you have looked for does not exist.', 'bucksmore'),
            'link_text' => __('Back to home page', 'bucksmore'),
        ],

        // General
        'general' => [
            'button_toggler' => __('Expand/toggle menu', 'bucksmore'),
        ],

        // Article Component
        'article' => [
            'edit' => __('Edit', 'bucksmore'),
        ],

        // Base Page
        'base' => [
            'no_content' => __('Sorry, no content', 'bucksmore'),
        ],

        // Comment Form Page
        'comment_form' => [
            'name'                => __('Name', 'bucksmore'),
            'email'               => __('Email', 'bucksmore'),
            'website'             => __('Website', 'bucksmore'),
            'comment'             => __('Comment', 'bucksmore'),
            'comment_placeholder' => __('Enter your comment here...', 'bucksmore'),
            'post_comment'        => __('Post Comment', 'bucksmore'),
            'reset'               => __('Reset', 'bucksmore'),
        ],

        // Comment Page
        'comment' => [
            'reply' => __('Reply', 'bucksmore'),
        ],

        // Slider
        'slider' => [
            'prev' => __('Previous', 'bucksmore'),
            'next' => __('Next', 'bucksmore'),
        ],

        // Search Page
        'search' => [
            'no_results' => __('Sorry, No Results. Try your search again.', 'bucksmore'),
        ],

        // Search Form Page
        'search_form' => [
            'search' => __('Type something', 'bucksmore'),
            'search_submit' => __('Search submit', 'bucksmore'),
        ],

        // Single Password Page
        'single_password' => [
            'password' => __('Password', 'bucksmore'),
            'submit'   => __('Submit', 'bucksmore'),
        ],

        // Single Page
        'single' => [
            'edit'     => __('Edit', 'bucksmore'),
            'comments' => __('Comments', 'bucksmore'),
        ],
    ];
}
